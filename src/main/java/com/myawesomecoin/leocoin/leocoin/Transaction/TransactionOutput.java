package com.myawesomecoin.leocoin.leocoin.Transaction;

import com.myawesomecoin.leocoin.leocoin.security.ShaUtil;

import java.security.PublicKey;

/**
 * Created by amitosh  on 17/02/18.
 */
public class TransactionOutput {

    public String id;
    public PublicKey reciepient;
    public float value;
    public String parentTransactionId;

    public TransactionOutput(PublicKey reciepient, float value, String parentTransactionId) {
        this.reciepient = reciepient;
        this.value = value;
        this.parentTransactionId = parentTransactionId;
        this.id = ShaUtil.applySha256(ShaUtil.getStringFromKey(reciepient)+Float.toString(value)+parentTransactionId);
    }


    public boolean isMine(PublicKey publicKey) {
        return (publicKey == reciepient);
    }
    public void setTransactionOutput(String id , TransactionOutput txo ){

    }

}
